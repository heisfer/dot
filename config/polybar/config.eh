;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================
[colors]
underline = #fffafa
;background = ${xrdb:color0:#222}
background = #ddff1023
background-alt = #444
;foreground = ${xrdb:color7:#222}
foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40
[bar/time]
width = 15%
height = 50
offset-x = 40%
offset-y = 50%
fixed-center = true
background = #6affffff
modules-center = battery

font-0 = fixed:pixelsize=11;2
font-1 = Ionicons:pixelsize=13;3

override-redirect = true
wm-restack = i3
[bar/top]
width = 100%
height = 27
offset-x = 1%
offset-y = 1%
fixed-center = true
line-size = 2
line-color = #f00
background = #5a000000
foreground = #eee
spacing = 2
padding-left = 1
padding-right = 1
module-margin-left = 1
module-margin-right = 1
font-0 = fixed:pixelsize=10;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = Wuncon Siji:pixelsize=10;1
modules-left = i3
modules-center = date
modules-right = filesystem volume xkeyboard memory cpu wlan eth battery temperature powermenu
;tray-position = right
;tray-padding = 2
;tray-transparent = true
;tray-background = #fff
;wm-restack = bspwm
;wm-restack = i3
;override-redirect = true
[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock
format-prefix = "? "
;format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}
label-layout = %layout%
label-layout-underline = ${colors.secondary}
label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}
[module/filesystem]
type = internal/fs
interval = 25
		
mount-0 = /
label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-mounted-underline = ${colors.underline}
[module/i3]
type = internal/i3
; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true
format-underline-size = 2
formal-padding = 2 
strip-wsnumbers = true
enable-click = false
enable-scroll = false
ws-icon-0 = 1;?
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;
ws-icon-9 = 0;
[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>
icon-prev = ?
icon-stop = ?
icon-play = ?
icon-pause = ?
icon-next = ?
label-song-maxlen = 25
label-song-ellipsis = true
[module/cpu]
type = internal/cpu
format-prefix = "? "
format-underline = ${colors.underline}
label = %percentage%%
[module/memory]
type = internal/memory
interval = 2
format-prefix = "? "
format-underline = ${colors.underline}
label = %percentage_used%%
[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3.0
format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.underline}
label-connected = %essid%
format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}
ramp-signal-0 = ?
ramp-signal-1 = ?
ramp-signal-2 = ?
ramp-signal-3 = ?
ramp-signal-4 = ?
[module/eth]
type = internal/network
interface = enp0s25
interval = 3.0
format-connected-underline = ${colors.underline}
format-connected-prefix = "? "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%
format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}
[module/date]
type = internal/date
interval = 5
date =
date-alt = " %Y-%m-%d"
time = %H:%M
time-alt = %H:%M:%S
format-prefix = ?
format-underline = ${colors.underline}
label = %date% %time%


[module/volume]
type = internal/alsa
format-volume = <ramp-volume> <label-volume> 
label-muted = sound muted
bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ?
bar-volume-fill-font = 2
bar-volume-empty = ?
bar-volume-empty-font = 2
ramp-volume-0 = 
ramp-volume-1 = 


[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
poll-interval = 5
time-format = %H:%M
format-charging = <animation-charging> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>
label-charging = %percentage%% %time%
label-discharging = %percentage%% %time%
ramp-capacity-0 = ?
ramp-capacity-0-foreground = #de0037
ramp-capacity-1 = ?
ramp-capacity-2 = ?
bar-capacity-width = 10
animation-charging-0 = ?
animation-charging-1 = ?
animation-charging-2 = ?
animation-charging-framerate = 750
;[module/battery]
;type = internal/battery
;battery = BAT0
;adapter = ADP1
;time-format = %H:%M
;full-at = = 100
;poll-interval = 5
;format-charging = <animation-charging> <label-charging>
;format-charging-underline = ${colors.underline}
;format-discharging = <ramp-capacity> <label-discharging> <time>
;format-discharging-underline = ${self.format-charging-underline}
;format-full-prefix = "? "
;format-full-prefix-foreground = ${colors.foreground-alt}
;format-full-underline = ${self.format-charging-underline}
;ramp-capacity-0 = ?
;ramp-capacity-0-foreground = #de0037
;ramp-capacity-1 = ?
;ramp-capacity-2 = ?
;ramp-capacity-foreground = ${colors.foreground-alt}
;
;animation-charging-0 = ?
;animation-charging-1 = ?
;animation-charging-2 = ?
;animation-charging-foreground = ${colors.foreground-alt}
;animation-charging-framerate = 750
[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60
format = <ramp> <label>
format-underline = ${colors.underline}
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}
label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}
ramp-0 = ?
ramp-1 = ?
ramp-2 = ?
[module/powermenu]
type = custom/menu
format-spacing = 1
label-open = ?
label-open-foreground = ${colors.secondary}
label-close = ? cancel
label-close-foreground = ${colors.secondary}
label-separator = |
menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2
menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = reboot
menu-2-0 = power off
menu-2-0-exec = shutdown now
menu-2-1 = cancel
menu-2-1-exec = menu-open-0
[settings]
screenchange-reload = false
;compositing-background = xor
compositing-background = over
;compositing-foreground = source
;compositing-border = over
compositing-underline = over
format-underline = #fffafa
[global/wm]
margin-top = 5
;margin-bottom = 5
; vim:ft=dosini

